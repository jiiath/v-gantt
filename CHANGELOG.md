# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

## [1.4.0](https://github.com/FEMessage/v-gantt/compare/v1.3.0...v1.4.0) (2020-11-11)


### Features

* add types ([#22](https://github.com/FEMessage/v-gantt/issues/22)) ([4588ed5](https://github.com/FEMessage/v-gantt/commit/4588ed5f698b9baec9f85cf108ed5888d4f6f7a3))

## [1.3.0](https://github.com/FEMessage/v-gantt/compare/v1.2.0...v1.3.0) (2020-09-10)


### Features

* support monthly view ([#18](https://github.com/FEMessage/v-gantt/issues/18)) ([5ccc1e7](https://github.com/FEMessage/v-gantt/commit/5ccc1e7e30d206a2ade0a24466a143b0d01f6682))

## [1.2.0](https://github.com/FEMessage/v-gantt/compare/v1.1.4...v1.2.0) (2020-09-01)


### Features

* 左侧 tree 支持 header slot 和 attrs ([#17](https://github.com/FEMessage/v-gantt/issues/17)) ([82f588e](https://github.com/FEMessage/v-gantt/commit/82f588e2b07b99532239a99e8832f74af9205b2f))

### [1.1.4](https://github.com/FEMessage/v-gantt/compare/v1.1.3...v1.1.4) (2020-08-07)

### [1.1.3](https://github.com/FEMessage/v-gantt/compare/v1.1.2...v1.1.3) (2020-07-10)


### Bug Fixes

* 暂时移除 esm 模块的导出 ([#11](https://github.com/FEMessage/v-gantt/issues/11)) ([70ccd2d](https://github.com/FEMessage/v-gantt/commit/70ccd2d6023363d4cfdf122e6ca28a5b0f594b04))

### [1.1.2](https://github.com/FEMessage/v-gantt/compare/v1.1.1...v1.1.2) (2020-07-10)


### Bug Fixes

* 修正 gren 依赖 ([#9](https://github.com/FEMessage/v-gantt/issues/9)) ([30a7f1c](https://github.com/FEMessage/v-gantt/commit/30a7f1c8c510619954d46acbc329c1efbca33c0f))

### [1.1.1](https://github.com/FEMessage/v-gantt/compare/v1.1.0...v1.1.1) (2020-07-10)


### Bug Fixes

* access public ([#6](https://github.com/FEMessage/v-gantt/issues/6)) ([4c4ee29](https://github.com/FEMessage/v-gantt/commit/4c4ee298424874bab420d8f86b7904fe8bb86e10))

## 1.1.0 (2020-07-10)


### Features

* license ([b555b0b](https://github.com/FEMessage/v-gantt/commit/b555b0b81e4d58ae6b30b0a89fed0c524a5e09f7))
* styleguide ([c3333a9](https://github.com/FEMessage/v-gantt/commit/c3333a9280167ee59d4ec2061a126cd885fe818a))
* 支持打包模式 ([f9554b9](https://github.com/FEMessage/v-gantt/commit/f9554b9938369fdaaf00f4102daa09e0d88f7fb1))
* 支持设置默认视图 ([d0ad96e](https://github.com/FEMessage/v-gantt/commit/d0ad96e737ecc4151a17880ce7594cd0e5fe3099))
* 组件代码迁移完成 ([a2dff13](https://github.com/FEMessage/v-gantt/commit/a2dff131bf8f8fe90d950489fde93a76aedf7fde))


### Bug Fixes

* todo in readme ([c0c352b](https://github.com/FEMessage/v-gantt/commit/c0c352b212d6459a371a22787f0ecc911e0bb3d1))
* yarn doc ([#3](https://github.com/FEMessage/v-gantt/issues/3)) ([9e2831f](https://github.com/FEMessage/v-gantt/commit/9e2831f6457c959154aacf9d60e00e77ea1d10d3))
* 内联样式 ([22f24db](https://github.com/FEMessage/v-gantt/commit/22f24db66c8cb62c087499ced57ff96384c56524))
* 开发环境不能 external 否则 vue-styleguide 报错 ([a5f2059](https://github.com/FEMessage/v-gantt/commit/a5f2059788bef05cd45b770a1d59d9086c16aeba))
* 手动内联 svg ([0dd03b1](https://github.com/FEMessage/v-gantt/commit/0dd03b197cfe0709e3ab11f4943c263e9fc0d859))
